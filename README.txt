CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module helps in searching the data from multisites using search_api_solr.
The backend uses, like the popular Apache Solr Search Integration module,
Apache Solr servers for indexing and searching content.

 * For a full description of the module visit:
   https://www.drupal.org/project/multisite_solr_search

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/multisite_solr_search


REQUIREMENTS
------------

This module requires search_api and search_api_solr modules.

https://www.drupal.org/project/search_api
https://www.drupal.org/project/search_api_solr


INSTALLATION
------------

 * Install the module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


CONFIGURATION
-------------

 * Navigate to Administration > Configuration > Search & Metadata and select a server.
 * Navigate to "Structure > Block Layout" and place "Multisite Solr Search" block in any region
   where you want to render the search block.


MAINTAINERS
-----------

 * sravya challa (sravya_12) - https://www.drupal.org/u/sravya_12

Supporting organization:

 * Valuebound - https://www.drupal.org/valuebound
